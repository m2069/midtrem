
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Win 10 Home
 */
public class ProducManage {

    private static ArrayList<Produc> produclist = new ArrayList<>();
   

//    private static Produc currentProduc = null;
    static {
        produclist = new ArrayList<>();
        //mock data
        produclist.add(new Produc("1", "Rice", "easy", 15.00, 1));
        produclist.add(new Produc("2", "Sausage", "Ceepee", 35.00, 1));
        produclist.add(new Produc("3", "Snack", "ArhanYodkhun", 10.5, 2));
    }

    static {
        load();
    }

    //add
    public static boolean addProduc(Produc produc) {
        produclist.add(produc);
        save();
        return true;
    }

    //remove
    public static boolean delProduc(Produc produc) {
        produclist.remove(produc);
        save();
        return true;
    }

    public static boolean delProduc(int index) {
        produclist.remove(index);
        save();
        return true;
    }

    //read
    public static ArrayList<Produc> getProduc() {
        return produclist;
    }

    public static Produc getProduc(int index) {
        return produclist.get(index);
    }

    // update
    public static boolean updateProduc(int index, Produc produc) {
        produclist.set(index, produc);
        save();
        return true;
    }

    //File
    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("nut.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(produclist);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProducManage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProducManage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("nut.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            produclist = (ArrayList<Produc>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProducManage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProducManage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProducManage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

//    static {
//        currentProduc(new Produc("1","Rice","easy",15.00,1);
//        addProduc(new Produc("2","Sausage","Ceepee",35.00,1));
//        addProduc(new Produc("3","Snack","ArhanYodkhun",10.5,2));
//    }
}
